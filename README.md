# bursterprecision1427

European Spallation Source ERIC Site-specific EPICS module: bursterprecision1427

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/Burster+Precision+Resistance+Decade+1427)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/bursterprecision1427-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
