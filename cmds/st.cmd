#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS - DataBase
# -----------------------------------------------------------------------------
# Burster Presicion Resistance Decade 1427
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@ess.eu
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# EPICS modules being loaded dynamically
# -----------------------------------------------------------------------------
require bursterprecision1427

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR, "172.30.32.11")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(LOCATION, "E04: $(IPADDR):$(IPPORT)")
epicsEnvSet(PREFIX, "Test:Rdec-Burster1427-01")
# SCAN is being used only for status (in the case of anything else presents a problem)
epicsEnvSet(SCAN, "5")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(bursterprecision1427_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

iocshLoad("$(bursterprecision1427_DIR)bursterprecision1427.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT), SCAN=$(SCAN)")

iocInit()

